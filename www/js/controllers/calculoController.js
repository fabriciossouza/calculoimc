angular.module('calculoImcApp')
.controller('calculoController', function($scope, $rootScope, $ionicPopup, $location, calculoImc, $state ) {

  $scope.dados = { };

  $scope.calcular = function (dados) {
    if (!dados.peso) {
      $ionicPopup.alert({ title: 'Alerta', template: 'Campo peso não informado' });
    } else if (!dados.altura) {
      $ionicPopup.alert({ title: 'Alerta', template: 'Campo altura não informado' });
    } else {
      var imc = calculoImc.calcular(dados.peso, dados.altura);
      var faixa = calculoImc.getFaixa(imc);

      $rootScope.imc = imc;
      $rootScope.faixa = faixa;
      $state.go("resultado" );
    }
  }
});