angular.module('calculoImcApp', ['ionic'])
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}).config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('calculo', {
      url: '/',
      cache: false,
      templateUrl: 'templates/calculo.html',
      controller: 'calculoController'
    })
    .state('resultado', {
      url: '/resultado',
      templateUrl: 'templates/resultado.html'
    })
    $urlRouterProvider.otherwise('/')
});